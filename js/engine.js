$(function() {

// Фиксированное меню при прокрутке страницы вниз
if ($(window).width() >= '768'){
$(window).scroll(function(){
        var sticky = $('.header-nav'),
            scroll = $(window).scrollTop();
        if (scroll > 200) {
            sticky.addClass('header-nav__fixed');
        } else {
            sticky.removeClass('header-nav__fixed');
        };
    });
}

// Валидация форм
jQuery.extend(jQuery.validator.messages, {
    required: "Заполните это поле",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});

$( ".form-question" ).validate( {
    highlight: function ( element, errorClass, validClass ) {
        $( element )
            .parents( ".form-item" )
            .addClass( "form-item__error" )
            .removeClass( "form-item__valid" );
    },

    unhighlight: function (element, errorClass, validClass) {
        $( element )
            .parents( ".form-item" )
            .addClass( "form-item__valid" )
            .removeClass( "form-item__error" );
    }
} );




// Мобильное меню
$('.mobile-toggler').click(function() {
    $(this).parent(".modal-menu__item--header").next().fadeToggle(0);
    $(".modal-menu__item--header").fadeToggle(0);
})

$('.modal-menu__back').click(function() {
    $(this).parent().fadeToggle(0);
    $(".modal-menu__item--header").fadeToggle(0);
})




// Стилизация селектов
$('select').styler();

// Фенсибокс
 $('.fancybox').fancybox({
   beforeShow : function(){
   $('select').styler('refresh');

  }

 });

 $('.fancybox.fancybox-bg__white').fancybox({
   beforeShow : function(){
    // Добавляем выпадающему в модальном окне меню белый фон
   $('.modal-menu:not(.no-bg)').parent().addClass('bg-fancybox__white');

  },
  afterClose : function(){
    $('body').removeClass('bg-fancybox__white');
  }

 });



// BEGIN of script for header submenu
$(".navbar-toggle").on("click", function () {
    $(this).toggleClass("active");
});



// Табы Физическое-Юридическое лицо
$('.person-item__individual').click(function(e) {
    e.preventDefault();
    $('.person-item').removeClass('active');
    $('.tabs-item').removeClass('active');
    $('#individual').addClass('active');
    $('.person-item__individual').addClass('active');
});
$('.person-item__entity').click(function(e) {
    e.preventDefault();
    $('.person-item').removeClass('active');
    $('.tabs-item').removeClass('active');
    $('#entity').addClass('active');
    $('.person-item__entity').addClass('active');
});





// Баннер слайдер
var bannerSlider = $('.banner-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: false,
    fade: true,
    adaptiveHeight: true
});
$('.banner-slider__prev').click(function(){
    $(bannerSlider).slick("slickPrev")
});
$('.banner-slider__next').click(function(){
    $(bannerSlider).slick("slickNext")
});




// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        inline: true,
        locale: 'ru',

    });
});


// Слайдер людей
$('.people-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: true,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 350,
      settings: {
        slidesToShow: 1
      }
    },
  ]
});

// Слайдер партнеров
$('.partners-slider').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: true,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    },
  ]
});





// BEGIN of script for back-to-top btn
    if ($('.back-to-top').length) {
        var scrollTrigger = 100, // px
        backToTop = function() {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('.back-to-top').addClass('show');
            } else {
                $('.back-to-top').removeClass('show');
            }
        };
        backToTop();
        $(window).on('scroll', function() {
            backToTop();
        });
        $('.back-to-top').on('click', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
// END of script for back-to-top btn




var photoHidden = $('.photo-hidden__slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    speed: 1000,
    dots: false,
    arrows: false,
    adaptiveHeight: true
});
$('.photo-hidden__prev').click(function(){
    $(photoHidden).slick("slickPrev")
});
$('.photo-hidden__next').click(function(){
    $(photoHidden).slick("slickNext")
});

$('.photo-item__img').click(function() {
    photoHidden.slick('refresh');
})












})